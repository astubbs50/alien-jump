﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    enum CharacterState { Idle, Walking, Jumping, Crouching, Falling };

    [Range(1f, 1000f)] [SerializeField] private float m_WalkSpeed = 40f;

    // How much to smooth out the movement
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;

    //The jump force
    [Range(10, 1000)] [SerializeField] private float m_JumpForce = 400f;

    //Grounded radius
    [Range(0, 1)] [SerializeField] private float k_GroundedRadius = 0.2f;

    //Is Grounded
    [SerializeField] private bool m_IsGrounded = true;

    [SerializeField] private LayerMask m_WhatIsGround;

    public AudioSource walkingSound;
    public AudioSource jumpingSound;
    public AudioSource damageSound;
    public AudioSource throwSound;
    public Transform m_GroundCheck;
    public GameObject m_GoDamage;
    public GameObject m_GoBomb;

    private Rigidbody2D m_Body;
    private float m_HorizontalMove = 0f;
    private CharacterState m_PlayerState = CharacterState.Idle;
    private CharacterState m_NextPlayerState = CharacterState.Idle;
    private Animator m_Animator;
    private Vector3 m_Velocity = Vector3.zero;
    private bool m_Jump = false;
    private bool m_GameOver = false;
    private float m_Health = 100f;
    private float m_MaxHealth = 100f;

    // For determining which way the player is currently facing.
    private bool m_FacingRight = true;

    // Start is called before the first frame update
    void Start()
    {
        m_Body = gameObject.GetComponent<Rigidbody2D>();
        m_Animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        bool isIdle = true;

        //Get movement input
        m_HorizontalMove = Input.GetAxisRaw("Horizontal") * m_WalkSpeed;

        //Change Player Direction
        if (m_HorizontalMove > 0 && !m_FacingRight)
        {
            Flip();
        }
        else if (m_HorizontalMove < 0 && m_FacingRight)
        {
            Flip();
        }

        //Set walking state
        if (m_PlayerState != CharacterState.Jumping && m_PlayerState != CharacterState.Falling)
        {
            if (Mathf.Abs(m_HorizontalMove) > 0)
            {
                m_NextPlayerState = CharacterState.Walking;
                isIdle = false;
            }
        }

        //Set the grounded boolean
        SetIsGrounded();

        //Set the Jumping State
        if (m_IsGrounded && Input.GetButtonDown("Jump") && m_PlayerState != CharacterState.Jumping)
        {
            m_Jump = true;
            isIdle = false;
            m_NextPlayerState = CharacterState.Jumping;
        }

        if(!m_IsGrounded)
        {
            m_NextPlayerState = CharacterState.Jumping;
            m_Animator.SetBool("isJumping", true);
        }
        else if (isIdle)
        {
            m_NextPlayerState = CharacterState.Idle;
        }

        //Throwing
        if (Input.GetButtonDown("Throw"))
        {
            if (Canvas.main.GetStars() > 0)
            {
                m_Animator.SetBool("isThrowing", true);
                ThrowBomb();
                Invoke("StopThrowing", 0.25f);
                Canvas.main.UpdateStars(-1);
            }
        }

        //Debug.Log("Is Idle: " + isIdle.ToString());
        if (isIdle && Input.GetButton("Duck"))
        {
            m_NextPlayerState = CharacterState.Crouching;            
        }

        SetPlayerState();
    }

    void FixedUpdate()
    {
        // Move the character by finding the target velocity
        Vector3 targetVelocity = new Vector2(m_HorizontalMove * Time.fixedDeltaTime, m_Body.velocity.y);

        // And then smoothing it out and applying it to the character
        m_Body.velocity = Vector3.SmoothDamp(m_Body.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

        // If the player should jump...
        if (m_Jump)
        {
            // Add a vertical force to the player.
            jumpingSound.Play();
            m_Body.AddForce(new Vector2(0f, m_JumpForce));
            m_Jump = false;
        }
    }

    void SetIsGrounded()
    {
        m_IsGrounded = false;
        Vector2 pointA = new Vector2(m_GroundCheck.position.x - 0.25f, m_GroundCheck.position.y - k_GroundedRadius);
        Vector2 pointB = new Vector2(m_GroundCheck.position.x + 0.25f, m_GroundCheck.position.y + k_GroundedRadius);
        Collider2D[] colliders = Physics2D.OverlapAreaAll(pointA, pointB, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_IsGrounded = true;
            }
        }
    }

    void SetPlayerState()
    {
        if (m_PlayerState != m_NextPlayerState)
        {
            switch (m_NextPlayerState)
            {
                case CharacterState.Walking:
                    StartWalking();
                    break;
                case CharacterState.Jumping:
                    StartJumping();
                    break;
                case CharacterState.Crouching:
                    StartDucking();
                    break;
                default:
                    StartIdle();
                    break;
            }
        }

        m_PlayerState = m_NextPlayerState;
    }

    void ResetAnimation()
    {
        m_Animator.SetBool("isJumping", false);
        m_Animator.SetBool("isWalking", false);
        m_Animator.SetBool("isFalling", false);
        m_Animator.SetBool("isDucking", false);
        m_Animator.SetBool("isThrowing", false);
    }

    void StartDucking()
    {
        m_Animator.SetBool("isDucking", true);
    }

    void StartIdle()
    {
        ResetAnimation();
    }

    void StartJumping()
    {
        if (m_IsGrounded)
        {
            ResetAnimation();
            m_Animator.SetBool("isJumping", true);
        }
    }

    void StopJumping()
    {
        m_Jump = false;
        m_Animator.SetBool("isJumping", false);
    }

    void StartWalking()
    {
        ResetAnimation();
        m_Animator.SetBool("isWalking", true);
    }

    void StopWalking()
    {
        m_Animator.SetBool("isWalking", false);
    }

    void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void ThrowBomb()
    {
        GameObject goNewBomb = Instantiate(m_GoBomb);
        Rigidbody2D bodyBomb = goNewBomb.GetComponent<Rigidbody2D>();
        float upForce = 200.0f;
        float newBombOffsetY = 0.0f;

        throwSound.Play();

        if (m_Animator.GetBool("isDucking"))
        {
            upForce = 0.0f;
            newBombOffsetY = -0.25f;
        }
        goNewBomb.transform.position = new Vector3(
            m_GoBomb.transform.position.x, m_GoBomb.transform.position.y + newBombOffsetY, m_GoBomb.transform.position.z);
        Vector2 throwForce = new Vector2(250.0f * gameObject.transform.localScale.x, upForce);

        goNewBomb.SetActive(true);
        bodyBomb.velocity = m_Body.velocity;
        bodyBomb.AddForce(throwForce);
    }

    private void StopThrowing()
    {
        m_Animator.SetBool("isThrowing", false);
    }

    public void CollectObject(string name)
    {
        if (name == "Star")
        {
            Canvas.main.UpdateStars(1);
        }
        else if(name == "Bottle")
        {
            //Canvas.main.SetHealth(m_MaxHealth);
            m_Health += 50f;
            if(m_Health > m_MaxHealth)
            {
                m_Health = m_MaxHealth;
            }
            Canvas.main.SetHealth(m_Health / m_MaxHealth);
        }
        else if (name.Length == 1)
        {
            Canvas.main.AddLetter(name);
        }
    }

    public void TakeDamage(float damage)
    {
        if (m_GameOver)
        {
            return;
        }
        m_Health -= damage;
        if (m_Health <= 0)
        {
            m_GameOver = true;
            m_Animator.SetBool("isFalling", true);
            m_Animator.SetBool("isJumping", false);
            m_Animator.SetBool("isWalking", false);
            Canvas.main.ShowGameOver();
            m_Health = 0;
            walkingSound.Stop();
            m_HorizontalMove = 0;
            gameObject.transform.Rotate(new Vector3(0, 0, 1), 90.0f);
        }
        Canvas.main.SetHealth(m_Health / m_MaxHealth);
        m_GoDamage.SetActive(true);
        Invoke("EndDamage", 0.25f);
        damageSound.Play();
    }

    private void EndDamage()
    {
        m_GoDamage.SetActive(false);
    }

    public void SetFall()
    {
        m_Animator.SetBool("isFalling", true);
        m_Animator.SetBool("isJumping", false);
    }

}
