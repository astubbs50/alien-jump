﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingItem : MonoBehaviour
{
    private float startY = 0f;
    private float duration = 1f;
    private float startOffset = 0f;
    private Transform rectTransform;

    // Use this for initialization
    void Start() {
        rectTransform = GetComponent<Transform>();
        startY = rectTransform.position.y;
        startOffset = Random.Range(0f, 1f);
    }

    // Update is called once per frame
    void Update() {
        var newY = startY + Mathf.Cos(Time.time + startOffset) * 0.1f;
        rectTransform.position = new Vector2(rectTransform.position.x, newY);
    }
}
