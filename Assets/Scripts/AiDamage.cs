﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiDamage : MonoBehaviour {

    [SerializeField]
    private float damage = 5.0f;

    [SerializeField]
    private float shockForce = 500.0f;

    private float shockDelay = 1f;
    private float shockLastTime;
    private float health = 1.0f;
    private bool isDead = false;

    private void Awake() {
        shockLastTime = Time.time;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (Time.time > shockLastTime + shockDelay) {
            if (collision.gameObject.tag == "Player") {
                collision.gameObject.GetComponent<Player>().TakeDamage(damage);
            }
            shockLastTime = Time.time;
        }
    }

    public void TakeDamage(float damage)
    {
        if (!isDead) {
            gameObject.AddComponent<Rigidbody2D>();
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0.0f, 100.0f));
            isDead = true;
        }
    }
}
