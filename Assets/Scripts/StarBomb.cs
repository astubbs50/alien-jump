﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarBomb : MonoBehaviour {
    public AudioSource explosionSound;

    private bool isExploding = false;
    private float expandRate = 1.05f;
    private float damage = 5.0f;

    private void OnEnable() {
        Invoke("Explode", 2.0f);
    }

    // Update is called once per frame
    void Update() {
        transform.Rotate(new Vector3(0, 0, -10.0f));
        if (isExploding) {
            if (transform.localScale.x < 2f) {
                transform.localScale = new Vector3(transform.localScale.x * expandRate, transform.localScale.y * expandRate, transform.localScale.z);
                expandRate += 0.02f;
            }
        }
    }

    void Explode() {
        isExploding = true;
        Destroy(gameObject, 0.3f);
        gameObject.GetComponent<Rigidbody2D>().Sleep();
        explosionSound.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Enemy") {
            collision.gameObject.GetComponent<AiDamage>().TakeDamage(damage);
            Explode();
        }
    }
}
