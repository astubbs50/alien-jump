﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSelector : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> levelCards;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (GameObject card in levelCards) {
            LevelCard lc = card.GetComponent<LevelCard>();
            if (PlayerPrefs.HasKey(lc.levelName) || lc.levelName == "Level_01") {
                lc.isLocked = false;
                lc.starsEarned = PlayerPrefs.GetInt(lc.levelName);
            } else {
                lc.isLocked = true;
            }
        }
    }

    public void ResetPlayerPrefs() {
        foreach (GameObject card in levelCards) {
            LevelCard lc = card.GetComponent<LevelCard>();
            if (PlayerPrefs.HasKey(lc.levelName)) {
                PlayerPrefs.DeleteKey(lc.levelName);
            }
        }
        SceneManager.LoadScene("Scene_Manager");
    }
}
