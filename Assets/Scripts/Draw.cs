﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Draw
{
    public static void Rectangle(Vector2 pointA, Vector2 pointB, Color color)
    {
        Vector2 tl = new Vector2(pointA.x, pointA.y);
        Vector2 tr = new Vector2(pointB.x, pointA.y);
        Vector2 dl = new Vector2(pointA.x, pointB.y);
        Vector2 dr = new Vector2(pointB.x, pointB.y);

        Debug.DrawLine(tl, tr, color);
        Debug.DrawLine(tr, dr, color);
        Debug.DrawLine(dl, dr, color);
        Debug.DrawLine(dl, tl, color);
    }

    public static void Circle(Vector2 center, float radius, Color color)
    {
        float angle = 0;
        float endAngle = Mathf.PI * 2;
        float stepAngle = Mathf.PI / 8;

        Vector2 last = new Vector2();
        bool first = true;

        while(angle < endAngle)
        {
            Vector2 pt = new Vector2();
            pt.x = center.x + Mathf.Cos(angle) * radius;
            pt.y = center.y + Mathf.Sin(angle) * radius;
            if(!first)
            {
                Debug.DrawLine(pt, last, color);
            }
            first = false;
            last = pt;
            angle += stepAngle;
        }
        
    }
}
