﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallPoint : MonoBehaviour
{
    private bool fadeOut = false;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<Player>().SetFall();
            Invoke("EndGame", 0.2f);
        }
    }

    private void EndGame() {
        if (!fadeOut) {
            fadeOut = true;
            Canvas.main.ShowGameOver();
            FollowPlayer.main.LockScreen();
        }
    }

    private void Update() {
        
    }
}
