﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorScript : MonoBehaviour
{
    [SerializeField]
    private string openWord = "cat";

    [SerializeField]
    private SpriteRenderer doorTop;

    [SerializeField]
    private SpriteRenderer doorBottom;

    public string levelName;
    public string nextLevel;

    private int total_stars;

    // Start is called before the first frame update
    void Start() {
        Canvas.main.RegisterWordDoor(this, openWord);
        total_stars = GameObject.FindGameObjectsWithTag("Star").Length;
    }

    public void OpenDoor() {
        doorTop.enabled = false;
        doorBottom.enabled = false;
        gameObject.GetComponent<CapsuleCollider2D>().enabled = true;
    }

    public void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            float current_stars = GameObject.FindGameObjectsWithTag("Star").Length;
            int earned_stars = Mathf.RoundToInt(((total_stars - current_stars) / total_stars) * 3);
            if(PlayerPrefs.HasKey(levelName)) {
                int last_stars = PlayerPrefs.GetInt(levelName);
                if(last_stars > earned_stars) {
                    earned_stars = last_stars;
                }
            }
            PlayerPrefs.SetInt(levelName, earned_stars);
            PlayerPrefs.SetInt(nextLevel, 0);
            SceneManager.LoadScene("Scene_Manager");
        }
    }
}
