﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform follow;
    public Transform background;
    public static FollowPlayer main;

    [SerializeField]
    private float backgroundScroll = 0.3f;

    [SerializeField]
    private float backgroundOffsetX = 0.0f;

    [SerializeField]
    private float backgroundOffsetY = 0.0f;

    private bool lockScreen = false;

    private void Awake() {
        main = this;
    }

    private void LateUpdate() {
        if(lockScreen) {
            return;
        }
        Vector2 diff = follow.position - transform.position;
        Vector2 movement = (diff) * Time.deltaTime;

        //Set the max diff to 4.0f
        if (diff.magnitude > 3.0f) {
            movement = diff - (diff.normalized * 3.0f);
            //Debug.Log("MAX SCROLL");
        }

        //Update camera transform position
        transform.position = new Vector3(
            transform.position.x + movement.x,
            transform.position.y + movement.y,
            transform.position.z);

        //Update background image position
        background.position = new Vector3(
            transform.position.x * backgroundScroll + backgroundOffsetX, 
            transform.position.y * backgroundScroll + backgroundOffsetY, 
            background.position.z);
    }

    public void LockScreen() {
        lockScreen = true;
    }

}
