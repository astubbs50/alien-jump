﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public AudioSource sound;

    private string name = "";

    private bool isDestroyed = false;

    private void Awake() {
        name = gameObject.tag;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player" && !isDestroyed) {
            collision.gameObject.GetComponent<Player>().CollectObject(name);
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject, 0.3f);
            sound.Play();
            isDestroyed = true;
        }
    }

}
