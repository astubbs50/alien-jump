﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Canvas : MonoBehaviour
{
    public static Canvas main;
    public Image healthBar;

    [SerializeField]
    private TextMeshProUGUI starsText;

    [SerializeField]
    private TextMeshProUGUI alphaText;

    [SerializeField]
    private TextMeshProUGUI healthText;

    [SerializeField]
    private GameObject gameOver;

    [SerializeField]
    private string levelText = "blank";

    [SerializeField]
    private Image fadeOut;

    private Dictionary<string, DoorScript> doors = new Dictionary<string, DoorScript>();
    private int stars;
    private bool fadingOut;
    private float a = 0;
    private bool bGameOver = false;

    void Awake() {
        main = this;
        stars = 0;
    }

    // Start is called before the first frame update
    void Start() {
        starsText.text = "Stars: " + stars.ToString();
        WordUp();
    }

    public void RegisterWordDoor(DoorScript door, string word) {
        doors.Add(word, door);
    }

    public void UpdateStars(int change) {
        stars += change;
        starsText.text = "Stars: " + stars.ToString();
    }

    public int GetStars() {
        return stars;
    }

    public void AddLetter(string letter) {
        //Assign the letter to our text
        alphaText.text += letter;
        WordUp();

        if(doors.ContainsKey(alphaText.text)) {
            doors[alphaText.text].OpenDoor();
        }
    }

    private void WordUp() {
        string temp = levelText;
        List<int> positions = new List<int>();
        alphaText.text.Replace("_", "");
        //Sort the characters by position in our level word
        foreach (char c in alphaText.text) {
            int pos = temp.IndexOf(c);
            if (pos >= 0) {
                positions.Add(pos);
                //Cross out current found char
                temp = temp.Substring(0, pos) + "_" + temp.Substring(pos + 1);
            }
        }

        //Reset the alpha text to the sorted word
        alphaText.text = "";
        for (int i = 0; i < levelText.Length; i++) {
            if (positions.Contains(i)) {
                alphaText.text += levelText[i];
            } else {
                alphaText.text += "_";
            }
        }
    }

    public void SetHealth(float amount) {
        healthBar.fillAmount = amount;
        healthText.text = Mathf.Round(amount * 100) + "%";
    }

    public void ShowGameOver() {
        gameOver.SetActive(true);
        fadingOut = true;
        Invoke("SetGameOver", 0.5f);
    }

    private void SetGameOver() {
        bGameOver = true;
    }

    private void Update() {
        if(fadingOut && a < 1.0f) {
            a += Time.deltaTime * 0.35f;
            fadeOut.color = new Color(fadeOut.color.r, fadeOut.color.g, fadeOut.color.b, a);
        }
        if(bGameOver && Input.anyKey) {
            SceneManager.LoadScene("Scene_Manager");
        }
    }

    public void GotoSceneManager() {
        SceneManager.LoadScene("Scene_Manager");
    }
}
