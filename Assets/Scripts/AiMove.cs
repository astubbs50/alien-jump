﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiMove : MonoBehaviour
{
    public List<Transform> groundChecks;
    public List<Transform> wallChecks;

    private float moveSpeed = 1.0f;

    private void Update()
    {
        //Make sure on solid ground
        foreach (Transform groundCheck in groundChecks) {
            Debug.DrawLine(groundCheck.position, transform.position);
            if (!Physics2D.OverlapCircle(groundCheck.position, 0.2f, 1 << LayerMask.NameToLayer("Ground"))) {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            }
        }

        //Check if a wall is ahead
        foreach (Transform wallCheck in wallChecks) {
            Debug.DrawLine(wallCheck.position, transform.position, Color.cyan);
            if (Physics2D.OverlapCircle(wallCheck.position, 0.1f, 1 << LayerMask.NameToLayer("Ground"))) {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            }
        }
    }

    private void FixedUpdate() 
    {
        gameObject.transform.position = new Vector3(
            gameObject.transform.position.x + moveSpeed * transform.localScale.x * -1 * Time.fixedDeltaTime, 
            gameObject.transform.position.y,
            gameObject.transform.position.z);
    }

}
