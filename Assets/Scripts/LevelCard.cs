﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelCard : MonoBehaviour
{
    public string levelName;
    public int starsEarned = 0;
    public bool isLocked;

    [SerializeField]
    private List<GameObject> starImages;

    [SerializeField]
    private GameObject lockImage;

    private void Start() {
        if (!isLocked) {
            lockImage.SetActive(false);
            for(int i = 0; i < starsEarned; i++) {
                Image img = starImages[i].GetComponent<Image>();
                img.color = Color.yellow;
            }
            var button = gameObject.AddComponent<Button>();
            button.onClick.AddListener(LevelSelected);
        }
    }

    public void LevelSelected() {
        if (!isLocked) {
            SceneManager.LoadScene(levelName);
        }
    }
}
