﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Blinking : MonoBehaviour
{
    private TextMeshProUGUI myText;

    // Start is called before the first frame update
    void Awake()
    {
        myText = gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        float alpha = Mathf.Abs(Mathf.Sin(Time.time * 2.0f) * 1f);
        //Debug.Log(alpha);
        myText.alpha = alpha;
    }
}
