﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour {
    public CharacterController2d controller;
    public Animator animator;
   
    public float runSpeed = 40f;
    public AudioSource walkingSound;
    public AudioSource jumpingSound;
    public AudioSource damageSound;
    public AudioSource throwSound;

    public GameObject goDamage;
    public GameObject goBomb;

    private float horizontalMove = 0f;
    private bool jump = false;
    private bool crouch = false;
    private float health = 100f;
    private float maxHealth = 100f;
    private Rigidbody2D body;
    private bool gameOver = false;
    const float walkSoundDelay = 0.315f;
    private float lastWalkSound = 0f;

    private void Start()
    {
        body = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() 
    {
        if(gameOver) {
            return;
        }
    
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if(Mathf.Abs(horizontalMove) > 0) 
        {
            animator.SetBool("isWalking", true);
            if(animator.GetBool("isJumping") || animator.GetBool("isFalling")) 
            {
                walkingSound.Stop();
            }
            else if (!walkingSound.isPlaying && lastWalkSound + walkSoundDelay < Time.time) 
            {
                walkingSound.Play();
                lastWalkSound = Time.time;
            }
        }
        else 
        {
            animator.SetBool("isWalking", false);
            walkingSound.Stop();
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (!animator.GetBool("isJumping"))
            {
                jumpingSound.Play();
            }
            jump = true;
            animator.SetBool("isJumping", true);
        }

        if (Input.GetButtonDown("Duck") && !animator.GetBool("isJumping"))
        {
            Debug.Log("Ducking");
            crouch = true;
            animator.SetBool("isDucking", true);
        }
        else if (Input.GetButtonUp("Duck") && controller.CheckIfCanStand()) 
        {
            crouch = false;
            Debug.Log("No Duck");
            animator.SetBool("isDucking", false);
        }

        if (Input.GetButtonDown("Throw"))
        {
            if (Canvas.main.GetStars() > 0) {
                animator.SetBool("isThrowing", true);
                ThrowBomb();
                Invoke("StopThrowing", 0.25f);
                Canvas.main.UpdateStars(-1);
            }
        }
    }

    private void ThrowBomb() 
    {
        GameObject goNewBomb = Instantiate(goBomb);
        Rigidbody2D bodyBomb = goNewBomb.GetComponent<Rigidbody2D>();
        float upForce = 200.0f;
        float newBombOffsetY = 0.0f;

        throwSound.Play();
        
        if (animator.GetBool("isDucking")) {
            upForce = 0.0f;
            newBombOffsetY = -0.25f;
        }
        goNewBomb.transform.position = new Vector3(
            goBomb.transform.position.x, goBomb.transform.position.y + newBombOffsetY, goBomb.transform.position.z);
        Vector2 throwForce = new Vector2(250.0f * gameObject.transform.localScale.x, upForce);

        
        goNewBomb.SetActive(true);
        bodyBomb.velocity = body.velocity;
        bodyBomb.AddForce(throwForce);
    }

    private void StopThrowing()
    {
        animator.SetBool("isThrowing", false);
    }

    private void FixedUpdate()
    {
        //Move our character
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);

        //Reset action variables
        jump = false;
    }

    public void CollectObject(string name) 
    {
        if(name == "Star") {
            Canvas.main.UpdateStars(1);
        } else if(name.Length == 1) {
            Canvas.main.AddLetter(name);
        }
    }

    public void SetFall() 
    {
        animator.SetBool("isFalling", true);
        animator.SetBool("isJumping", false);
    }

    public void TakeDamage(float damage) 
    {
        if (gameOver) {
            return;
        }
        health -= damage;
        if(health <= 0) {
            gameOver = true;
            animator.SetBool("isFalling", true);
            animator.SetBool("isJumping", false);
            animator.SetBool("isWalking", false);
            Canvas.main.ShowGameOver();
            health = 0;
            walkingSound.Stop();
            horizontalMove = 0;
            gameObject.transform.Rotate(new Vector3(0, 0, 1), 90.0f);
        }
        Canvas.main.SetHealth(health / maxHealth);
        goDamage.SetActive(true);
        Invoke("EndDamage", 0.25f);
        damageSound.Play();
    }

    private void EndDamage() 
    {
        goDamage.SetActive(false);
    }

    public void OnLanding() 
    {
        Debug.Log("Landed");
        animator.SetBool("isJumping", false);
        animator.SetBool("isFalling", false);
    }

    public void OnDucking(bool isDucking)
    {
        animator.SetBool("isDucking", isDucking);
        //animator.SetBool("isJumping", false);
        //animator.SetBool("isFalling", false);
    }
}
